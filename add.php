<?php
require_once('lib/app.php');

//intialize data variables
$full_name = '';
$email = '';
$password = '';
$gender = '';
$gender = '';
$city = '';
$hobbies = '';
$about_me = '';
$user_type = 'GENERAL';
//set user type ADMIN if no user in db
$query = "SELECT count(*) as total_user FROM user";
$result = mysqli_query($link, $query);
$users = array();
$row = mysqli_fetch_assoc($result);
$total_user = intval($row['total_user']);
if($total_user === 0){
	$user_type = 'ADMIN';
}
//dd($user_type);

//hold full_name
if(isset($_POST['full_name']))
	$full_name = $_POST['full_name'];
//hold email
if(isset($_POST['email']))
	$email = $_POST['email'];
//hold password
if(isset($_POST['password']))
	$password = $_POST['password'];
//hold gender
if(isset($_POST['gender']))
	$gender = $_POST['gender'];
//hold city
if(isset($_POST['city']))
	$city = $_POST['city'];
//hold hobbies
if(isset($_POST['hobbies']) && !empty($_POST['hobbies']))
	$hobbies = implode(',',$_POST['hobbies']);
//hold about_me
if(isset($_POST['about_me']))
	$about_me = $_POST['about_me'];
if(isset($_POST['user_type']))
	$user_type = $_POST['user_type'];


$photo_tmp = $_FILES['photo']['tmp_name'];
$photo_id = uniqid();
$photo_type = $_FILES['photo']['type'];
$photo_path = 'photo/'.$photo_id;
move_uploaded_file($photo_tmp, $photo_path);
$query = "INSERT INTO user(user_type,full_name, email, password, gender, city, hobbies, about_me, photo_path) 
			values('".$user_type."','".$full_name."','".$email."','".$password."','".$gender."','".$city."','".$hobbies."','".$about_me."','".$photo_path."')";
//dd($query);

$success = mysqli_query($link,$query);
if($success){
	$_SESSION['msg_success'] = "Data is successfully added";
}else{
	$_SESSION['msg_error'] = "Failed to add data";
}
header('location: index.php');
