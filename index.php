<?php
	require_once('lib/app.php');
	$query = "SELECT * FROM user";
	$result = mysqli_query($link, $query);
	$users = array();
	while($row = mysqli_fetch_assoc($result)){
		$users[] = $row;
	}
?>
<!DOCTYPE html>
<html>
<head>
	<title>List of users</title>
</head>
<body>


<nav>
	<li><a href="create.php">Registration</a></li>
	<?php if(user_loggedin()):?>
		<li><a href="dashboard.php">My Dashboard</a></li>
	<?php else: ?>
		<li><a href="login.php">Login</a></li>
	<?php endif?>
</nav>
<h1>List of Users</h1>
<table border="1">
	<thead>
		<tr>
			<th>Sl.</th>
			<th>id</th>
			<th>user type</th>
			<th>photo</th>
			<th>full name</th>
			<th>email</th>
			<th>gender</th>
			<th>city</th>
			<th>hobbies</th>
			<th>about_me</th>
			<th>actions</th>
		</tr>
	</thead>
	<tbody>
	<?php if(count($users)): $sl = 0;?>
		<?php foreach($users as  $user):?>
		<tr>
			<td><?php echo ++$sl; ?></td>
			<td><?php echo $user['id'] ?></td>
			<td><?php echo $user['user_type'] ?></td>
			<td><img src="<?php echo $user['photo_path']; ?>" height='100px' width="100px"></td>
			<td><?php echo $user['full_name'] ?></td>
			<td><?php echo $user['email'] ?></td>
			<td><?php echo $user['gender'] ?></td>
			<td><?php echo $user['city'] ?></td>
			<td><?php echo $user['hobbies'] ?></td>
			<td><?php echo $user['about_me'] ?></td>
			<td>
				<a href="delete.php?id=<?php echo $user['id'] ?>"> delete</a> |
				<a href="view.php?id=<?php echo $user['id'] ?>">view</a> |
				<a href="edit.php?id=<?php echo $user['id'] ?>">Edit</a>
			</td>
		</tr>
		<?php endforeach;?>
	<?php else:?>
		<tr><td colspan="9">No user found!</td></tr>
	<?php endif;?>
	</tbody>
</table>
</body>
</html>