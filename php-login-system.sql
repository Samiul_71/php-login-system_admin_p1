/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50620
Source Host           : localhost:3306
Source Database       : php-login-system

Target Server Type    : MYSQL
Target Server Version : 50620
File Encoding         : 65001

Date: 2015-09-13 03:03:00
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_type` varchar(255) NOT NULL,
  `full_name` varchar(100) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(128) NOT NULL,
  `gender` varchar(20) NOT NULL,
  `city` varchar(30) NOT NULL,
  `hobbies` varchar(255) DEFAULT NULL,
  `about_me` text,
  `photo_path` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('44', 'ADMIN', 'à¦¬à§‹à¦°à¦¹à¦¾à¦¨ à¦šà§Œà¦§à§à¦°à§€', 'borhan.chittagong@gmail.com', 'qaz', 'male', 'Chittagong', 'Coding,Singing,Sleeping', 'I am nobody', 'photo/55f48f3e944db');
INSERT INTO `user` VALUES ('45', 'ADMIN', 'à¦¶à¦¾à¦¨à§à¦¤à¦¿', 'shanta@gmail.com', 'qaz', 'female', 'Dhaka', 'Gardening,Sleeping,Reading', 'I am sweet. Is not I?', 'photo/55f48f8f46bf3');
INSERT INTO `user` VALUES ('46', 'GENERAL', 'à¦¶à¦¾à¦®à¦¿à¦®à§à¦² à¦¸à§ˆà¦•à¦¤', 'sikat@gmail.com', 'qaz', 'male', 'Dhaka', 'Coding,Sleeping,Reading', 'I am the CR', 'photo/55f4904092ffb');
