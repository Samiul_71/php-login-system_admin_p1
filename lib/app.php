<?php
/*
*
* 
*
*/
session_start();
require_once('debug.php');
require_once('database.php');
require_once('functions.php');

$config = array();
$config['cities'] = array('Chittagong','Dhaka','Barisal','Khulna','Rangpur');
$config['hobbies'] = array('Gardening'=>'I Love Gardening', 
						'Coding' => 'I love to code',
						'Singing' => 'I love to sing',
						'Sleeping' => 'I love to sleep',
						'Reading' => 'I love to read books');
$config['photo_path'] = 'profile_photo/';
$_SESSION['config'] = $config;
?>

<p style="border:1px solid green"><b>Current User:</b> <?php show_user_info();?></p>
