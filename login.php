<?php
	include('lib/app.php');
	if(isset($_SESSION['user_loggedin']) && $_SESSION['user_loggedin'] == true){
		header('location: dashboard.php');
	}
?>
<!DOCTYPE html>
<html>
<head>
	<title>create</title>
</head>
<body>
<!-- include success or error msgs -->
<?php include('lib/msg.php');?>

<nav>
	<li><a href="index.php">Home</a></li>
</nav>

<!-- login form -->
<form action="check.php" method="post">
	<fieldset>
		<legend>Login</legend>
		
		<!-- email -->
		<div>
			<label for="email">Email</label>
			<input type="text" name="email" id="email"value="">
		</div>
		<!-- password -->
		<div>
			<label for="password">Password</label>
			<input type="password" name="password" id="password"value="">
		</div>

		
		<!-- submit -->
		<input type="submit" value="Sign In">
	</fieldset>
</form>

</body>
</html>