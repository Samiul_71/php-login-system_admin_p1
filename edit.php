<?php
	require_once('lib/app.php');
	$id = $_GET['id'];

	$query = "SELECT * FROM user WHERE id=".$id;
	$result = mysqli_query($link, $query);
	$user = array();
	while($row = mysqli_fetch_assoc($result)){
		$row['hobbies'] = explode(",", $row['hobbies']);
		$user = $row;

	}

	$config = $_SESSION['config'];
	$cities = $config['cities'];
	$hobbies = $config['hobbies'];
	 
?>

<!DOCTYPE html>
<html>
<head>
	<title>Edit user</title>
</head>
<body>
<nav>
	<li><a href="index.php">Home</a></li>
</nav>
<form action="modify.php?id=<?php echo $id;?>" method="post">
	<fieldset>
		<legend>Edit user info</legend>
		<!-- user type: only ADMIN can view -->
		<?php if(get_user_type() == 'ADMIN'):?>
		<div>
			<label for="user_type">user type</label>
			<select name="user_type" id="user_type">
				<option  value="GENERAL" <?php if($user['user_type']=='GENERAL') echo 'selected';?>>GENERAL</option>
				<option  value="ADMIN" <?php if($user['user_type']=='ADMIN') echo 'selected';?>  >ADMIN</option>
			</select>
		</div>
		<?php endif?>
		<!-- name -->
		<div>
			<label for="txtFullName">Full name</label>
			<input type="text" name="full_name" id="txtFullName"value="<?php echo $user['full_name'];?>">
		</div>
		<!-- email -->
		<div>
			<label for="email">Email</label>
			<input type="text" name="email" id="email" value="<?php echo $user['email'];?>">
		</div>
		<div>
			<label>Gender</label>
			<input type="Radio" name="gender" id="optGender1" value="male" <?php if($user['gender'] == 'male'){echo 'checked';}?> />
			<label for="optGender1">Male</label>

			<input type="Radio" name="gender" id="optGender2" value="female" <?php if($user['gender'] == 'female'){echo 'checked';}?>>
			<label for="optGender2">Female</label>
		</div>

		<!-- city -->
		<div>
			<label for="city">Select City</label>
			<select name="city" id="city">
			<?php foreach ($cities as $city): ?>
				<option value="<?php echo $city;?>" <?php if($user['city'] == $city) echo "selected";?>><?php echo $city;?></option>
			<?php endforeach;?>
			</select>
		</div>

		<!-- hobbies -->
		<div>
			<label >Select Your Hobbies</label>
			<?php foreach($hobbies as $hobby_value => $hobby_label):?>
				<p>
					<input type="checkbox" name="hobbies[]" value="<?php echo $hobby_value;?>" <?php if(in_array($hobby_value, $user['hobbies'])){echo "checked";}?>> 
					<?php echo $hobby_label; ?>
				</p>
			<?php endforeach; ?>
		</div>

		<!-- about me -->
		<div>
			<label for="about_me">About me</label>
			<textarea name="about_me" id="about_me"><?php echo $user['about_me'];?></textarea>
		</div>
		
		<!-- profile img -->
		<div>
			<div>
				<img src="<?php echo $user['photo_path']; ?>" height='100px' width="100px">
			</div>
			<label for="photo">Change Your Photo</label>
			<input type="file" name="photo">
		</div>

		

		<input type="submit" value="Save Info">
	</fieldset>
</form>

</body>
</html>